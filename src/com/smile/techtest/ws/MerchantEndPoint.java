package com.smile.techtest.ws;

import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.smile.techtest.pojo.DeleteMerchantRequest;
import com.smile.techtest.pojo.DeleteMerchantResponse;
import com.smile.techtest.pojo.GetMerchantRequest;
import com.smile.techtest.pojo.GetMerchantResponse;
import com.smile.techtest.pojo.PostMerchantRequest;
import com.smile.techtest.pojo.PostMerchantResponse;
import com.smile.techtest.pojo.UpdateMerchantRequest;
import com.smile.techtest.pojo.UpdateMerchantResponse;
import com.smile.techtest.service.MerchantService;

@Endpoint
public class MerchantEndPoint {
	private static final String NAMESPACE_URI = "https://www.smileandpay.com/sp/schemas";

	private MerchantService merchantService;

	@Autowired
	public void merchantEndpoint(MerchantService merchantService) throws JDOMException {
		this.merchantService = merchantService;
		Namespace namespace = Namespace.getNamespace("hr", NAMESPACE_URI);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetMerchantRequest")
	@ResponsePayload
	public GetMerchantResponse getMerchantRequest(@RequestPayload GetMerchantRequest merchantRequest) throws Exception {
		GetMerchantResponse response = new GetMerchantResponse();
		response.setMerchant(merchantService.getMerchant(merchantRequest.getId()));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "PostMerchantRequest")
	@ResponsePayload
	public PostMerchantResponse postMerchantRequest(@RequestPayload PostMerchantRequest merchantRequest)
			throws Exception {
		PostMerchantResponse response = new PostMerchantResponse();
		response.setMerchant(merchantService.saveMerchant(merchantRequest.getMerchant()));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "UpdateMerchantRequest")
	@ResponsePayload
	public UpdateMerchantResponse updateMerchantRequest(@RequestPayload UpdateMerchantRequest merchantRequest)
			throws Exception {
		UpdateMerchantResponse response = new UpdateMerchantResponse();
		response.setMerchant(merchantService.updateMerchant(merchantRequest.getMerchant()));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "DeleteMerchantRequest")
	@ResponsePayload
	public DeleteMerchantResponse deleteMerchantRequest(@RequestPayload DeleteMerchantRequest merchantRequest)
			throws Exception {
		DeleteMerchantResponse response = new DeleteMerchantResponse();
		merchantService.deleteMerchant(merchantService.getMerchant(merchantRequest.getId()));
		response.setAnswer("OK");
		return response;
	}
}
