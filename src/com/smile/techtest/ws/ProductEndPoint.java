package com.smile.techtest.ws;

import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.smile.techtest.jpa.MerchantJPA;
import com.smile.techtest.pojo.AssociateProductRequest;
import com.smile.techtest.pojo.AssociateProductResponse;
import com.smile.techtest.pojo.DeleteProductRequest;
import com.smile.techtest.pojo.DeleteProductResponse;
import com.smile.techtest.pojo.GetProductRequest;
import com.smile.techtest.pojo.GetProductResponse;
import com.smile.techtest.pojo.Merchant;
import com.smile.techtest.pojo.PostProductRequest;
import com.smile.techtest.pojo.PostProductResponse;
import com.smile.techtest.pojo.Product;
import com.smile.techtest.pojo.UpdateProductRequest;
import com.smile.techtest.pojo.UpdateProductResponse;
import com.smile.techtest.service.MerchantService;
import com.smile.techtest.service.ProductService;

@Endpoint
public class ProductEndPoint {
	private static final String NAMESPACE_URI = "https://www.smileandpay.com/sp/schemas";

	private ProductService productService;
	private MerchantService merchantService;

	@Autowired
	public void productEndpoint(ProductService productService, MerchantService merchantService) throws JDOMException {
		this.productService = productService;
		this.merchantService = merchantService;
		Namespace namespace = Namespace.getNamespace("hr", NAMESPACE_URI);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetProductRequest")
	@ResponsePayload
	public GetProductResponse getProductRequest(@RequestPayload GetProductRequest productRequest) throws Exception {
		GetProductResponse response = new GetProductResponse();
		response.setProduct(productService.getProduct(productRequest.getId()));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "PostProductRequest")
	@ResponsePayload
	public PostProductResponse postProductRequest(@RequestPayload PostProductRequest productRequest) throws Exception {
		PostProductResponse response = new PostProductResponse();
		response.setProduct(productService.saveProduct(productRequest.getProduct()));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "UpdateProductRequest")
	@ResponsePayload
	public UpdateProductResponse updateProductRequest(@RequestPayload UpdateProductRequest productRequest)
			throws Exception {
		UpdateProductResponse response = new UpdateProductResponse();
		response.setProduct(productService.updateProduct(productRequest.getProduct()));
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "DeleteProductRequest")
	@ResponsePayload
	public DeleteProductResponse deleteProductRequest(@RequestPayload DeleteProductRequest productRequest)
			throws Exception {
		DeleteProductResponse response = new DeleteProductResponse();
		productService.deleteProduct(productService.getProduct(productRequest.getId()));
		response.setAnswer("OK");
		return response;
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "AssociateProductRequest")
	@ResponsePayload
	public AssociateProductResponse associateProductRequest(@RequestPayload AssociateProductRequest productRequest)
			throws Exception {
		AssociateProductResponse response = new AssociateProductResponse();
//		Merchant merc = merchantService.getMerchant(productRequest.getMerchantId());
//		Product prod = productService.getProduct(productRequest.getProductId());
//		prod.getMerchants().add(merc);
//		merc.getProducts().add(prod);
//		productService.updateProduct(prod);
		
//		merchantService.updateMerchant(merc);
		productService.associate(productRequest.getMerchantId(), productRequest.getProductId());
		response.setAnswer("OK");
		return response;
	}
}
