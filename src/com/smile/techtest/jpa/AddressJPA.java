package com.smile.techtest.jpa;

import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.smile.techtest.pojo.Address;

@Entity
@Table(name = "address")
public class AddressJPA {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private int number;

	private String street;

	@ManyToOne(cascade = CascadeType.PERSIST, optional = false)
	@JoinColumn(name = "merchant_id", nullable = false)
	private MerchantJPA merchant;

	public int getId() {
		return id;
	}

	public MerchantJPA getMerchant() {
		return merchant;
	}

	public void setMerchant(MerchantJPA merchant) {
		this.merchant = merchant;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	private String zipcode;

	public static Address toAddress(AddressJPA addr) {
		Address address = new Address();

		address.setId(addr.getId());
		address.setNumber(addr.getNumber());
		address.setStreet(addr.getStreet());
		address.setZipcode(addr.getZipcode());

		return address;
	}

	public static AddressJPA fromAddress(Address addr) {
		AddressJPA address = new AddressJPA();
		if (addr != null) {
			if (addr.getId() != null) {
				address.setId(addr.getId());
			}
			if (addr.getNumber() != null) {
				address.setNumber(addr.getNumber());
			}
			address.setStreet(addr.getStreet());
			address.setZipcode(addr.getZipcode());
		}
		return address;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddressJPA a = (AddressJPA) obj;
		return Objects.equals(getId(), a.getId());
	}

}
