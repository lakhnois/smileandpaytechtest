package com.smile.techtest.jpa.logic;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.smile.techtest.jpa.MerchantJPA;

public class MerchantEntityManager {
	static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("SmileTechTest");
	static EntityManager entityManager;
	
	private static MerchantEntityManager instance;
	
	static {
		entityManager =  entityManagerFactory.createEntityManager();
		instance = new MerchantEntityManager();
	}
	
	public static MerchantEntityManager getInstance() {
		return instance;
	}
	
	
	public MerchantJPA find(Integer id) {
		MerchantJPA merc = (MerchantJPA)entityManager.find(MerchantJPA.class, id);
		return merc;
	}
	
	public void save(MerchantJPA merchant) {
		EntityTransaction transac = entityManager.getTransaction();
		transac.begin();
		try {
			entityManager.persist(merchant);
			entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			transac.commit();
		}
	}
	
	public void update(MerchantJPA merchant) {
		EntityTransaction transac = entityManager.getTransaction();
		transac.begin();
		try{
			entityManager.merge(merchant);
			entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			transac.commit();
		}
	}
	
	public void delete(MerchantJPA merchant) {
		EntityTransaction transac = entityManager.getTransaction();
		transac.begin();
		try {
			entityManager.remove(entityManager.merge(merchant));
			entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace(System.err);
		} finally {
			transac.commit();
		}
	}
}
