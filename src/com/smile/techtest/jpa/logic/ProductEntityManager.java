package com.smile.techtest.jpa.logic;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.smile.techtest.jpa.AddressJPA;
import com.smile.techtest.jpa.MerchantJPA;
import com.smile.techtest.jpa.ProductJPA;

public class ProductEntityManager {
	static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("SmileTechTest");
	static EntityManager entityManager;
	
	private static ProductEntityManager instance;
	
	static {
		entityManager =  entityManagerFactory.createEntityManager();
		instance = new ProductEntityManager();
	}
	
	public static ProductEntityManager getInstance() {
		return instance;
	}
	
	
	public ProductJPA find(Integer id) {
		ProductJPA prod = (ProductJPA)entityManager.find(ProductJPA.class, id);
		return prod;
	}
	
	public void save(ProductJPA product) {
		EntityTransaction transac = entityManager.getTransaction();
		transac.begin();
		try {
			entityManager.persist(product);
			entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			transac.commit();
		}
	}
	
	public void update(ProductJPA product) {
		EntityTransaction transac = entityManager.getTransaction();
		transac.begin();
		try{
			entityManager.merge(product);
			entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			transac.commit();
		}
	}
	
	public void delete(ProductJPA product) {
		EntityTransaction transac = entityManager.getTransaction();
		transac.begin();
		try{
			entityManager.remove(entityManager.merge(product));
			entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			transac.commit();
		}
	}
	
	public void associate(Integer merchantId, Integer productId) {
		EntityTransaction transac = entityManager.getTransaction();
		transac.begin();
		try{
			MerchantJPA merc = (MerchantJPA)entityManager.find(MerchantJPA.class, merchantId);
			ProductJPA prod = (ProductJPA)entityManager.find(ProductJPA.class, productId);
			
			merc.getProducts().add(prod);
			entityManager.merge(prod);
			entityManager.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			transac.commit();
		}
		
	}
}
