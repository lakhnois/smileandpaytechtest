package com.smile.techtest.jpa;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import com.smile.techtest.pojo.Merchant;
import com.smile.techtest.pojo.Product;

/**
 * @author ludo
 *
 */
@Entity
@Table(name = "product")
public class ProductJPA {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "create_date")
	private Timestamp createDate;

	private String label;

	@Column(name = "unitprice")
	private float unitPrice;

	private String currency;

	private float weight;

	private float height;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinTable(name = "asso_merchant_product", joinColumns = @JoinColumn(name = "product_id"), inverseJoinColumns = @JoinColumn(name = "merchant_id"))
	private List<MerchantJPA> merchants = new ArrayList<MerchantJPA>();

	public List<MerchantJPA> getMerchants() {
		return merchants;
	}

	public void setMerchants(List<MerchantJPA> merchants) {
		this.merchants = merchants;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public float getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public static Product toProduct(ProductJPA product) {
		return toProduct(product, true);
	}

	public static Product toProduct(ProductJPA product, boolean includeLists) {
		Product prod = new Product();

		if (product != null) {
			prod.setId(product.getId());
			if (product.getCreateDate() != null) {
				GregorianCalendar cal = new GregorianCalendar();
				cal.setTime(product.getCreateDate());
				try {
					prod.setCreateDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
				} catch (DatatypeConfigurationException e) {
					e.printStackTrace();
				}
			}

			prod.setUnitPrice(BigDecimal.valueOf(product.getUnitPrice()));
			prod.setCurrency(product.getCurrency());
			prod.setHeight(BigDecimal.valueOf(product.getHeight()));
			prod.setWeight(BigDecimal.valueOf(product.getWeight()));
			prod.setLabel(product.getLabel());

			if (includeLists) {
				for (MerchantJPA merc : product.getMerchants()) {
					prod.getMerchants().add(MerchantJPA.toMerchant(merc, false));
				}
			}
		}

		return prod;
	}

	public static ProductJPA fromProduct(Product product) {
		return fromProduct(product, true);
	}

	public static ProductJPA fromProduct(Product product, boolean includeLists) {
		ProductJPA prod = new ProductJPA();

		if (product != null) {
			if (product.getId() != null) {
				prod.setId(product.getId());
			}

			if (product.getCreateDate() != null) {
				prod.setCreateDate(new Timestamp(product.getCreateDate().toGregorianCalendar().getTime().getTime()));
			}

			prod.setUnitPrice(product.getUnitPrice().floatValue());
			prod.setCurrency(product.getCurrency());
			prod.setHeight(product.getHeight().floatValue());
			prod.setWeight(product.getWeight().floatValue());
			prod.setLabel(product.getLabel());

			if (includeLists) {
				prod.setMerchants(new ArrayList<MerchantJPA>());
				for (Merchant merc : product.getMerchants()) {
					prod.getMerchants().add(MerchantJPA.fromMerchant(merc, false));
				}
			}
		}

		return prod;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductJPA p = (ProductJPA) obj;
		return Objects.equals(getId(), p.getId());
	}

}
