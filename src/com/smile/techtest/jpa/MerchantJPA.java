package com.smile.techtest.jpa;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import com.smile.techtest.pojo.Address;
import com.smile.techtest.pojo.Merchant;
import com.smile.techtest.pojo.Product;

@Entity
@Table(name = "merchant")
public class MerchantJPA {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String name;

	private String lastname;

	@Column(name = "create_date")
	private Timestamp createDate;

	private Date birthdate;

	@OneToMany(mappedBy = "merchant", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<AddressJPA> addresses = new ArrayList<AddressJPA>();

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinTable(name = "asso_merchant_product", joinColumns = @JoinColumn(name = "merchant_id"), inverseJoinColumns = @JoinColumn(name = "product_id"))
	private List<ProductJPA> products = new ArrayList<ProductJPA>();

	public List<ProductJPA> getProducts() {
		return products;
	}

	public void setProducts(List<ProductJPA> products) {
		this.products = products;
	}

	public List<AddressJPA> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<AddressJPA> addresses) {
		this.addresses = addresses;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public static Merchant toMerchant(MerchantJPA merc) {
		return toMerchant(merc, true);
	}

	public static Merchant toMerchant(MerchantJPA merc, boolean includeLists) {
		Merchant merchant = new Merchant();

		merchant.setId(merc.getId());
		merchant.setName(merc.getName());
		merchant.setLastname(merc.getLastname());

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(merc.getCreateDate());
		try {
			merchant.setCreateDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		cal.setTime(merc.getBirthdate());
		try {
			merchant.setBirthdate(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}

		if (includeLists) {
			for (AddressJPA add : merc.getAddresses()) {
				merchant.getAddresses().add(AddressJPA.toAddress(add));
			}

			for (ProductJPA prod : merc.getProducts()) {
				merchant.getProducts().add(ProductJPA.toProduct(prod, false));
			}
		}

		return merchant;
	}

	public static MerchantJPA fromMerchant(Merchant merc) {
		return fromMerchant(merc, true);
	}

	public static MerchantJPA fromMerchant(Merchant merc, boolean includeLists) {
		MerchantJPA merchant = new MerchantJPA();

		if (merc != null) {
			if (merc.getId() != null) {
				merchant.setId(merc.getId());
			}
			merchant.setName(merc.getName());
			merchant.setLastname(merc.getLastname());

			if (merc.getBirthdate() != null) {
				merchant.setBirthdate(new Date(merc.getBirthdate().toGregorianCalendar().getTime().getTime()));
			}

			if (merc.getCreateDate() != null) {
				merchant.setCreateDate(new Timestamp(merc.getCreateDate().toGregorianCalendar().getTime().getTime()));
			}

			merchant.setAddresses(new ArrayList<AddressJPA>());

			if (includeLists) {
				for (Address add : merc.getAddresses()) {
					AddressJPA addjpa = AddressJPA.fromAddress(add);
					addjpa.setMerchant(merchant);
					merchant.getAddresses().add(addjpa);
				}

				merchant.setProducts(new ArrayList<ProductJPA>());
				for (Product prod : merc.getProducts()) {
					merchant.getProducts().add(ProductJPA.fromProduct(prod, false));
				}
			}
		}

		return merchant;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MerchantJPA m = (MerchantJPA) obj;
		return Objects.equals(getId(), m.getId());
	}
}
