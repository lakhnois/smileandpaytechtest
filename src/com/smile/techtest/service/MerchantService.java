package com.smile.techtest.service;

import com.smile.techtest.pojo.Merchant;

public interface MerchantService {
	
	public Merchant getMerchant(Integer id);
	
	public Merchant saveMerchant(Merchant merchand);
	
	public Merchant updateMerchant(Merchant merchant);
	
	public void deleteMerchant(Merchant merchant);
	
}
