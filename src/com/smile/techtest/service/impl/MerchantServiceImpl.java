package com.smile.techtest.service.impl;

import java.sql.Timestamp;

import com.smile.techtest.jpa.MerchantJPA;
import com.smile.techtest.jpa.logic.MerchantEntityManager;
import com.smile.techtest.pojo.Merchant;
import com.smile.techtest.service.MerchantService;

public class MerchantServiceImpl implements MerchantService {

	@Override
	public Merchant getMerchant(Integer id) {
		MerchantJPA merc =  MerchantEntityManager.getInstance().find(id);
		if (merc == null) return null;
		return MerchantJPA.toMerchant(merc);
	}
	
	public Merchant saveMerchant(Merchant merchant) {
		MerchantJPA merc = MerchantJPA.fromMerchant(merchant);
		merc.setCreateDate(new Timestamp((new java.util.Date()).getTime()));
		MerchantEntityManager.getInstance().save(merc);
		
		merchant = MerchantJPA.toMerchant(merc);
		return merchant;
	}
	
	public Merchant updateMerchant(Merchant merchant) {
		MerchantJPA merc = MerchantJPA.fromMerchant(merchant);
		MerchantEntityManager.getInstance().update(merc);
		
		merchant = MerchantJPA.toMerchant(merc);
		return merchant;
	}
	
	public void deleteMerchant(Merchant merchant) {
		MerchantJPA merc = MerchantJPA.fromMerchant(merchant);
		MerchantEntityManager.getInstance().delete(merc);
	}
	
}
