package com.smile.techtest.service.impl;

import java.sql.Timestamp;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.smile.techtest.jpa.ProductJPA;
import com.smile.techtest.jpa.logic.ProductEntityManager;
import com.smile.techtest.pojo.Product;
import com.smile.techtest.service.ProductService;

public class ProductServiceImpl implements ProductService {

	@Override
	public Product getProduct(Integer id) {
		ProductJPA prod =  ProductEntityManager.getInstance().find(id);
		if (prod == null) return null;
		return ProductJPA.toProduct(prod);
	}
	
	public Product saveProduct(Product product) {
		ProductJPA prod = ProductJPA.fromProduct(product);
		prod.setCreateDate(new Timestamp((new java.util.Date()).getTime()));
		ProductEntityManager.getInstance().save(prod);
		
		product = ProductJPA.toProduct(prod);
		return product;
	}
	
	public Product updateProduct(Product product) {
		ProductJPA prod = ProductJPA.fromProduct(product);
		ProductEntityManager.getInstance().update(prod);
		
		product = ProductJPA.toProduct(prod);
		return product;
	}
	
	public void deleteProduct(Product product) {
		ProductJPA prod = ProductJPA.fromProduct(product);
		ProductEntityManager.getInstance().delete(prod);
	}
	
	public void associate(Integer merchantId, Integer productId) {
		ProductEntityManager.getInstance().associate(merchantId, productId);
	}
	
}
