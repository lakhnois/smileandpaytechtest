package com.smile.techtest.service;

import com.smile.techtest.pojo.Product;

public interface ProductService {
	
	public Product getProduct(Integer id);
	
	public Product saveProduct(Product product);
	
	public Product updateProduct(Product product);
	
	public void deleteProduct(Product product);
	
	public void associate(Integer merchantId, Integer productId);
	
}
