//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.02.19 à 09:43:58 PM CET 
//


package com.smile.techtest.pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="merchant_id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="product_id" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "merchantId",
    "productId"
})
@XmlRootElement(name = "AssociateProductRequest")
public class AssociateProductRequest {

    @XmlElement(name = "merchant_id")
    protected int merchantId;
    @XmlElement(name = "product_id")
    protected int productId;

    /**
     * Obtient la valeur de la propriété merchantId.
     * 
     */
    public int getMerchantId() {
        return merchantId;
    }

    /**
     * Définit la valeur de la propriété merchantId.
     * 
     */
    public void setMerchantId(int value) {
        this.merchantId = value;
    }

    /**
     * Obtient la valeur de la propriété productId.
     * 
     */
    public int getProductId() {
        return productId;
    }

    /**
     * Définit la valeur de la propriété productId.
     * 
     */
    public void setProductId(int value) {
        this.productId = value;
    }

}
