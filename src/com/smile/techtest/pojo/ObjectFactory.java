//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2021.02.19 à 09:43:58 PM CET 
//


package com.smile.techtest.pojo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.smile.techtest.pojo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.smile.techtest.pojo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetMerchantRequest }
     * 
     */
    public GetMerchantRequest createGetMerchantRequest() {
        return new GetMerchantRequest();
    }

    /**
     * Create an instance of {@link GetMerchantResponse }
     * 
     */
    public GetMerchantResponse createGetMerchantResponse() {
        return new GetMerchantResponse();
    }

    /**
     * Create an instance of {@link Merchant }
     * 
     */
    public Merchant createMerchant() {
        return new Merchant();
    }

    /**
     * Create an instance of {@link PostMerchantRequest }
     * 
     */
    public PostMerchantRequest createPostMerchantRequest() {
        return new PostMerchantRequest();
    }

    /**
     * Create an instance of {@link PostMerchantResponse }
     * 
     */
    public PostMerchantResponse createPostMerchantResponse() {
        return new PostMerchantResponse();
    }

    /**
     * Create an instance of {@link UpdateMerchantRequest }
     * 
     */
    public UpdateMerchantRequest createUpdateMerchantRequest() {
        return new UpdateMerchantRequest();
    }

    /**
     * Create an instance of {@link UpdateMerchantResponse }
     * 
     */
    public UpdateMerchantResponse createUpdateMerchantResponse() {
        return new UpdateMerchantResponse();
    }

    /**
     * Create an instance of {@link DeleteMerchantRequest }
     * 
     */
    public DeleteMerchantRequest createDeleteMerchantRequest() {
        return new DeleteMerchantRequest();
    }

    /**
     * Create an instance of {@link DeleteMerchantResponse }
     * 
     */
    public DeleteMerchantResponse createDeleteMerchantResponse() {
        return new DeleteMerchantResponse();
    }

    /**
     * Create an instance of {@link GetProductRequest }
     * 
     */
    public GetProductRequest createGetProductRequest() {
        return new GetProductRequest();
    }

    /**
     * Create an instance of {@link GetProductResponse }
     * 
     */
    public GetProductResponse createGetProductResponse() {
        return new GetProductResponse();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link PostProductRequest }
     * 
     */
    public PostProductRequest createPostProductRequest() {
        return new PostProductRequest();
    }

    /**
     * Create an instance of {@link PostProductResponse }
     * 
     */
    public PostProductResponse createPostProductResponse() {
        return new PostProductResponse();
    }

    /**
     * Create an instance of {@link UpdateProductRequest }
     * 
     */
    public UpdateProductRequest createUpdateProductRequest() {
        return new UpdateProductRequest();
    }

    /**
     * Create an instance of {@link UpdateProductResponse }
     * 
     */
    public UpdateProductResponse createUpdateProductResponse() {
        return new UpdateProductResponse();
    }

    /**
     * Create an instance of {@link DeleteProductRequest }
     * 
     */
    public DeleteProductRequest createDeleteProductRequest() {
        return new DeleteProductRequest();
    }

    /**
     * Create an instance of {@link DeleteProductResponse }
     * 
     */
    public DeleteProductResponse createDeleteProductResponse() {
        return new DeleteProductResponse();
    }

    /**
     * Create an instance of {@link AssociateProductRequest }
     * 
     */
    public AssociateProductRequest createAssociateProductRequest() {
        return new AssociateProductRequest();
    }

    /**
     * Create an instance of {@link AssociateProductResponse }
     * 
     */
    public AssociateProductResponse createAssociateProductResponse() {
        return new AssociateProductResponse();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

}
