http://localhost:8081/smile/product/

<x:Envelope xmlns:x = "http://schemas.xmlsoap.org/soap/envelope/" 
   xmlns:tns = "https://www.smileandpay.com/sp/schemas">
   <x:Header/>
   <x:Body>
      <tns:UpdateProductRequest>
         <tns:product id="1" label="Plus gros produit" unit_price="20" currency="EUR" weight="10" height="0.22" create_date="2021-02-19+01:00" >
         </tns:product>
      </tns:UpdateProductRequest>
   </x:Body>
</x:Envelope>