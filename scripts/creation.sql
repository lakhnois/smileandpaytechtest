CREATE TABLE IF NOT EXISTS merchant (
    id serial PRIMARY KEY,
    create_date TIMESTAMP NOT NULL,
    name VARCHAR(50) NOT NULL,
    lastname VARCHAR(50) NOT NULL,
    birthdate DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS product (
    id serial PRIMARY KEY,
    create_date TIMESTAMP NOT NULL,
    label VARCHAR(100) NOT NULL,
    unitprice NUMERIC NOT NULL,
    currency VARCHAR(3) NOT NULL,
    weight NUMERIC(5, 2) NOT NULL,
    height NUMERIC (4, 2) NOT NULL
);

CREATE TABLE IF NOT EXISTS address (
    id serial PRIMARY KEY,
    merchant_id INT NOT NULL,
    number INT NOT NULL,
    street VARCHAR(50) NOT NULL,
    zipcode VARCHAR(10) NOT NULL,
    CONSTRAINT fk_merchant_addr
        FOREIGN KEY(merchant_id) 
	        REFERENCES merchant(id)
);

CREATE TABLE IF NOT EXISTS asso_merchant_product (
    merchant_id INT NOT NULL,
    product_id INT NOT NULL,
    create_date TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,
    CONSTRAINT fk_merchant_prod
        FOREIGN KEY(merchant_id) 
	        REFERENCES merchant(id),
    CONSTRAINT fk_product
        FOREIGN KEY(product_id) 
	        REFERENCES product(id),
    UNIQUE (merchant_id, product_id)
);